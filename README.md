# PROJET COVID


## Objectifs du projet

Projet COVID est un projet qui a pour objectif de permettre la consultation 
de données liées à la propagation du coronavirus dans le monde.
On retrouve des données telles que le nombre de décès, le nombre de cas
actifs, les taux de mortalité et d'incidence, et bien plus encore.
Nous avons aussi voulu permettre leur accès dans différents référentiels. 
Ainsi, on peut accéder à des données générales (dans le monde) ou plus spécifiques
(dans un continent, un pays, une province ou un comté).
Les données sont représentées de différentes façons : on les retrouve sous forme
textuelle et graphique (carte et diagrammes).

## Initialisation Du Projet

Pour lancer le projet, il est préférable d'utiliser l'IDE Pycharm de JetBrains
>[Pycharm](https://www.jetbrains.com/fr-fr/pycharm/)



Pour récupérer le code et l'exécuter, veuillez suivre les étapes suivantes (dans l'ordre).
1) Vous devez tout d'abord créer un nouveau projet Django dans Pycharm que vous appellerez "projet_covid".
Vous allez ensuite supprimer le package projetcovid et le fichier manage.py qui viennent d'être créés.


2) Vous devez ensuite créer un repository git et cloner le projet de cette url dans un nouveau dossier.
>[clone](https://forge.univ-lyon1.fr/p1602970/projet_covid)

Une fois cette étape réalisée, vous verrez apparaitre dossier nommé "projet_covid" qui contient : home/, 
projetcovid/, et plusieurs autres fichiers.

3) Vous allez devoir déplacer tous les dossiers et fichiers contenus dans le dossier obtenu à l'étape 2 (home/, 
   projetcovid/, etc...) vers le dossier créé à l'étape 1 (projet_covid).
Maintenant que ce répertoire est vide (celui de l'étape 2), vous pouvez le supprimer. 


4) Nous allons désormais nous occuper de l'environnement virtuel. 
Tout d'abord, vous devez vous assurer que votre environnement virtuel soit activé. 
Si ce n'est pas le cas, utilisez la commande ci-dessous (dans projet_covid) : 
```
source env/bin/activate
```

Pour que votre environnement virtuel soit complet vous devez :
* installer pandas (pip3 install pandas)
* installer le package request (pip3 install requests)
* installer le package sqlachemy (pip install Flask-SQLAlchemy)
* installer le package sklearn (pip install sklearn)


5) Enfin, lancez le script de création de la base de données avec la commande :
```
python3 initDatabase.py
```


6) Vous pouvez maintenant lancer le projet avec la commande ci-dessous : 
```
python manage.py runserver
```

## Vue de l'application

Une fois que le code a fini de compiler, vous pouvez accéder au serveur en collant cette adresse dans le navigateur de votre choix:
```
http://127.0.0.1:8000/
```








