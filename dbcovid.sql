DROP TABLE Quantification;
DROP TABLE Localisation;

CREATE TABLE Localisation(
    [Comte] varchar[50],
	[Province_State] varchar[50],
	[Country_Region] varchar[50] not null,
	[Latitude] float,
	[Longitude] float,
	[Combined_Key] object primary key not null
);

CREATE TABLE Quantification(
	[id_quant] integer primary key autoincrement not null,
	[Confirmed] integer,
	[Deaths] integer,
	[Recovered] integer,
	[Active] integer,
	[Incidence_Rate] float,
	[Case_Fatality_Ratio] float,
	[Date] date not null,
	[Combined_Key] object references Localisation(Combined_Key) on update cascade
);


