from django.shortcuts import render
import sqlite3
from home.modeles.importDonneesBasiques import *
from home.modeles.importcsv import DailyQuant
from home.modeles.requetesPourMap import recupPaysDeath
from home.modeles.requetesPourChart import recupDeathDernierMois
from home.modeles.requetesPourViews import recupListePays, recupListeProv, recupListeComte
from home.modeles.requetesPourViews import getCombinedKeyPays, getCombinedKeyProv, getCombinedKeyComte

#pour afficher la page d'index
def index(request):
        conn = sqlite3.connect('covid.db')
        cursor = conn.cursor()
        listePays = recupListePays(cursor)

        #on fait les requetes pour la map
        dicoPaysDeaths, dicoException = recupPaysDeath(cursor)
        listDeath, listDate, listIR = recupDeathDernierMois(cursor)

        listeDonnees = importDonneesBasiquesAvg(cursor)
        context = {'listePays': listePays,
                   'dicoPaysDeaths': dicoPaysDeaths,
                   'dicoException': dicoException,
                   'listeDeath': listDeath,
                   'listeDate': listDate,
                   'listeIR': listIR}
        context.update(listeDonnees)

        conn.close()

        return render(request, 'home/index.html', context)

#pour afficher la page de pays
def pays(request, pays):
        conn = sqlite3.connect('covid.db')
        cursor = conn.cursor()
        listeProv = recupListeProv(cursor, pays)

        #si il n'y a pas de provinces
        if (len(listeProv) == 1 and listeProv[0] == pays) or len(listeProv) == 0:
                combKey = getCombinedKeyPays(cursor, pays)
                listeLatLong, dicoDonnees = importDonneesBasiques(cursor, combKey)
                context = {'com': pays,
                            'prov': pays,
                            'pays': pays,
                            'nbSousCateg':0}
                context.update(listeLatLong)
                context.update(dicoDonnees)
                return render(request, 'home/comte.html', context)

        listeDonnees = importDonneesBasiquesProvinceAvg(cursor, pays)
        context = {'pays': pays,
                   'listeProv': listeProv}
        context.update(listeDonnees)

        conn.close()
        return render(request, 'home/pays.html', context)

#pour afficher la page des provinces
def province(request, pays, prov):
        conn = sqlite3.connect('covid.db')
        cursor = conn.cursor()
        listeCom = recupListeComte(cursor, prov)

        #si il y a pas de comptes
        if (len(listeCom) == 1 and listeCom[0] == prov) or len(listeCom) == 0:
                combKey = getCombinedKeyProv(cursor, pays, prov)
                listeLatLong, dicoDonnees = importDonneesBasiques(cursor, combKey)

                context = {'com': prov,
                           'prov': prov,
                           'pays': pays,
                           'nbSousCateg':1}
                context.update(listeLatLong)
                context.update(dicoDonnees)
                return render(request, 'home/comte.html', context) #page finale

        listeDonnees = importDonneesBasiquesComteAvg(cursor, pays, prov)
        context = {'pays': pays,
                   'prov': prov,
                   'listeCom': listeCom}
        context.update(listeDonnees)

        conn.close()
        return render(request, 'home/province.html', context)

#pour afficher la page de comte
def comte(request, pays, prov, com):
        conn = sqlite3.connect('covid.db')
        cursor = conn.cursor()
        combKey = getCombinedKeyComte(cursor, prov, com)
        listeLatLong, dicoDonnees = importDonneesBasiques(cursor, combKey)
        conn.close()

        context = {'com': com,
                   'prov': prov,
                   'pays': pays,
                   'nbSousCateg':2}
        context.update(listeLatLong)
        context.update(dicoDonnees)
        return render(request, 'home/comte.html', context)




