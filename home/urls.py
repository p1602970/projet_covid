from django.urls import path
from . import views

app_name = 'home'
urlpatterns = [
    path('', views.index, name='index'),
    path('<str:pays>/', views.pays, name='pays'),
    path('<str:pays>/<str:prov>/', views.province, name='province'),
    path('<str:pays>/<str:prov>/<str:com>/', views.comte, name='comte'),
]