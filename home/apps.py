from django.apps import AppConfig
from home.modeles.importcsv import importatio
from home.modeles.importcsv import DailyQuant
from home.modeles.importcsv import prediction

class HomeConfig(AppConfig):
    name = 'home'

    def ready(self):
        importatio()
        DailyQuant()
        prediction()
