"""
    Dans ce fichier, nous allons récupérer des données necessaires à l'affichage des
graphiques.
    On veut 2 graphiques : un qui représente l'évolution du nombre de décès dans le monde
au cours des 30 derniers jours, et un qui représente l'évolution du taux d'incidence
dans le monde ces 30 derniers jours.
    Il nous faut donc 3 listes : une avec les dates (tous les 3 jours), une avec le
nombre de décès à chaque date, et une avec le taux d'incidence à chaque date.
"""

import datetime

def recupDeathDernierMois(cursor):

    cursor.execute("select max(Date) from Quantification")
    maxDate = cursor.fetchall()[0][0]
    maxDate = datetime.datetime.strptime(maxDate, '%Y-%m-%d %H:%M:%S.%f')

    date = maxDate

    listDate = []
    listDeaths = []
    listIR = []

    #on va chercher les valeurs espacées de 3 jours sur 1 mois (30 jours)
    #donc on va 10 fois dans la boucle
    for i in range(1, 11):

        #par ex, si on est le 1er decembre, date.day donnera 1, nous on veut 01 pour la requete
            #donc on corrige
        if len(str(date.day)) == 1:
            day = '0' + str(date.day)
        else:
            day = str(date.day)

        #pareil avec les mois
        if len(str(date.month)) == 1:
            month = '0' + str(date.month)
        else:
            month = str(date.month)

        #on prepare la date dans un format adapté à la requete
        d = "'" + str(date.year) + '-' + month + '-' + day + "%'"

        querry = "select sum(Deaths), avg(Incidence_Rate) from Quantification " \
                 "where Date like " + d + ";"
        cursor.execute(querry)
        res = cursor.fetchall()[0]
        deaths = res[0]
        ir = res[1]
        listDate.insert(0, day + '-' + str(date.month))
        listDeaths.insert(0, deaths)
        listIR.insert(0, ir)
        date += datetime.timedelta(days=-3)

    return listDeaths, listDate, listIR

