#recupère la Combined_Key d'un pays sans provinces
def getCombinedKeyPays(cursor, pays):
    # on cherche la COmbined_Key
    cursor.execute("select Combined_Key from Localisation "
                   "where  Country_Region = '{pays}'".format(pays=pays))
    ck = cursor.fetchall()
    ck = ck[0][0]
    return ck

#recupère la Combined_Key d'une province sans comtés
def getCombinedKeyProv(cursor, pays, prov):
    cursor.execute("select Combined_Key from Localisation "
                   "where  Province_State = '{prov}'".format(prov=prov))
    combKey = cursor.fetchall()
    if combKey == []:
        combKey = getCombinedKeyProvNull(cursor, pays)
    combKey = combKey[0][0]
    return combKey

#fonction utilisée quand on clique sur la France ou le Danemark, sur la carte uniquement
#si on clique sur la France par exemple, on veut aller sur la page avec les données de la France
    #on veut pas les données regroupées de la France et de ses outres-mers
#On cherche la France métropolitaine uniquement
def getCombinedKeyProvNull(cursor, pays):
    cursor.execute("select Combined_Key from Localisation "
                   "where  Province_State is null and Country_Region = '{pays}'".format(pays=pays))
    combKey = cursor.fetchall()
    return combKey

#recupère la Combined_Key d'un comté
def getCombinedKeyComte(cursor, prov, com):
    cursor.execute("select Combined_Key from Localisation "
                   "where Comte = '{com}' and Province_State = '{prov}'".format(com=com, prov=prov))
    combKey = cursor.fetchall()
    combKey = combKey[0][0]
    return combKey

#récupère la liste de tous les pays
def recupListePays(cursor):
    cursor.execute("select distinct Country_Region from Localisation")
    listePays = cursor.fetchall()
    for i, tuple in enumerate(listePays):
        listePays[i] = tuple[0]
    return listePays

#récupère la liste des province/state d'un pays donné
def recupListeProv(cursor, pays):
    cursor.execute("select distinct Province_State from Localisation "
                   "where Country_Region = '{c}'".format(c=pays))
    listeProv = cursor.fetchall()
    for i, tuple in enumerate(listeProv):
        if type(tuple[0]) == str:
            listeProv[i] = tuple[0]
        else:
            listeProv[i] = pays
    return listeProv

#récupère la liste des comtés d'une province donnée
def recupListeComte(cursor, prov):
    cursor.execute("select Comte from Localisation "
                   "where Province_State = '{c}'".format(c=prov))
    listeCom = cursor.fetchall()

    for i, tuple in enumerate(listeCom):
        if type(tuple[0]) == str:
            listeCom[i] = tuple[0]
        else:
            listeCom[i] = prov
    return listeCom