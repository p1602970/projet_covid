"""
    Nous définissons dans ce fichier des fonctions qui font des requêtes,
traitent leurs résultats et renvoient des listes de données.

    Dans le cas d'un lieu précis (c'est à dire, un pays sans province/state, une
province/state sans comté, ou un comté), on prend les données brutes. Cela concerne
uniquement la fonction "importDonneesBasiques(cursor, Combined_key)."

    Dans le cas d'un lieu non précis (qui a donc des "sous-lieus"), les données
générales à ce lieu n'existeront pas. On fait donc la somme des valeurs numériques
(comme le nombre de décès, ou le nombre de cas confirmés), et la moyenne des taux
(taux de mortalité et taux d'incidence).

"""


#récupère les données qui correpondent à la combined key
def importDonneesBasiques(cursor, Combined_Key):
    #récupère la latitude et la longitude
    cursor.execute("select Latitude, Longitude from Localisation where Combined_key = '{ck}'".format(ck = Combined_Key))
    listel = cursor.fetchall()
    listel = {'Latitude': listel[0][0], 'Longitude': listel[0][1]}

    #récupère plus de données
    cursor.execute("select * from Quantification "
                        "where Combined_Key = '{ck}' and "
                   "Date = (select max(Date) from Quantification "
                        "where Combined_Key = '{ck}')".format(ck=Combined_Key))
    listeq = cursor.fetchall()
    listeq = listeq[0]
    donnees = {'Confirmed': listeq[1],
                'Deaths': listeq[2],
                'Recovered': listeq[3],
                'Active': listeq[4],
                'Incidence_Rate': listeq[5],
                'Case_Fatality_Ratio': listeq[6],
                'Date': listeq[7].split(' ')[0]}

    return listel, donnees


#récupère les données sommées ou moyennes du pays fournie en paramètre
def importDonneesBasiquesProvinceAvg(cursor, pays):
    cursor.execute("select distinct Date, sum(Confirmed), sum(Deaths), sum(Recovered), sum(Active), avg(Incidence_Rate), avg(Case_Fatality_Ratio)"
                   "from Localisation natural join Quantification "
                   "where Country_Region = '{pays}' "
                   "and Date = (Select max(Date) from Quantification )"
                   .format(pays=pays))
    d = cursor.fetchall()
    d = d[0]

    donnees = {'Date': d[0].split(' ')[0],
               'Confirmed': d[1],
               'Deaths': d[2],
               'Recovered': d[3],
               'Active': d[4],
               'Incidence_Rate': d[5],
               'Case_Fatality_Ratio': d[6]
         }

    return donnees

#récupère les données sommées ou moyennes de la province fournie en paramètre
def importDonneesBasiquesComteAvg(cursor, pays, prov):
    cursor.execute("select distinct Date, sum(Confirmed), sum(Deaths), sum(Recovered), sum(Active), avg(Incidence_Rate), avg(Case_Fatality_Ratio)"
                   "from Localisation natural join Quantification "
                   "where Country_Region = '{pays}' "
                   "and Province_State = '{prov}'"
                   "and Date = (Select max(Date) from Quantification )"
                   .format(pays=pays, prov=prov))
    d = cursor.fetchall()
    print(d)
    d = d[0]

    donnees = {'Date': d[0].split(' ')[0],
               'Confirmed': d[1],
               'Deaths': d[2],
               'Recovered': d[3],
               'Active': d[4],
               'Incidence_Rate': d[5],
               'Case_Fatality_Ratio': d[6]
         }

    return donnees

#récupère les données sommées ou moyennes dans le monde
def importDonneesBasiquesAvg(cursor):
    cursor.execute("select distinct Date, sum(Confirmed), sum(Deaths), sum(Recovered), sum(Active), avg(Incidence_Rate), avg(Case_Fatality_Ratio)"
                   "from Localisation natural join Quantification "
                   "where Date = (Select max(Date) from Quantification )")
    d = cursor.fetchall()
    print(d)
    d = d[0]

    donnees = {'Date': d[0].split(' ')[0],
               'Confirmed': d[1],
               'Deaths': d[2],
               'Recovered': d[3],
               'Active': d[4],
               'Incidence_Rate': d[5],
               'Case_Fatality_Ratio': d[6]
         }

    return donnees