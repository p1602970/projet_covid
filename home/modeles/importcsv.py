"""
    Ce fichier python permet l'importation des données en ligne vers notre base.
    Il permet le remplissage de la base, ou sa mise à jour si elle est déjà remplie
    Pour cela, on se base sur le nombre de tuples dans la table Localisation:
    - si il n'y a pas de tuples, la table est vide, donc on doit importer toutes les
données, depuis aujourd'hui jusqu'à la date à partir de laquelle on veut des données,
    - si il y a déjà des tuples, la base a déjà été remplie. On va donc chercher la
date jusqu'à laquelle elle est remplie, et la compléter avec les données disponibles
depuis cette date là.

    A plusieurs moments, nous utilisons des exceptions pour trouver la date des dernières
données mise en ligne. Il s'agit soit de la veille, soit de l'avant-veille (cela dépend
de l'heure). Ainsi, on regarde si des données pour la veille existent. Si ce n'est pas le
cas, on cherchera les données de l'avant-veille (qui existent forcément).
"""

import pandas as pd
import io
import requests
import datetime
from sqlalchemy import create_engine
import sqlite3
from sklearn.linear_model import LinearRegression


# pip install Flask-SQLAlchemy
# dans venv

# appelé quand Localisation est vide
def importationLocVersDb(sqlite_connection, date):
    # on récupère le dataframe loc, et on le met dans la table
    loc = prepareLoc(date)
    loc.to_sql('Localisation', sqlite_connection, if_exists='append', index=False)


# appelé quand localisation n'est pas vide, et qu'on veut completer la table
def majLoc(sqlite_connection, date):
    # on essaye de récupérer le dataframe loc
    # si on réussit, on essaye d'inserer les tuples de loc dans la base
    # si le tuple existe déjà, on raise exception et on pass
    # si il existe pas, il est insérer
    try:
        loc = prepareLoc(date)
    except KeyError:
        # il n'y a pas de données pour cette date
        return False

    for i in range(len(loc) + 1):
        try:
            loc.iloc[i:i + 1].to_sql('Localisation', sqlite_connection, if_exists='append', index=False)
        except:
            # le tuple existe déjà dans la base
            pass
    return True


# recupère les données d'une certaine date
#  et les met en forme pour l'insertion dans la base
def prepareLoc(date):
    url = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/" + date + ".csv"
    s = requests.get(url).content
    loc = pd.read_csv(io.StringIO(s.decode("utf-8")), delimiter=',')


    # on renomme les colonnes pour qu'elles correspondent à la base de données
    loc = loc.rename(columns={"Lat": "Latitude",
                              "Long_": "Longitude",
                              "Case-Fatality_Ratio": "Case_Fatality_Ratio",
                              "Incident_Rate": "Incidence_Rate",
                              "Admin2": "Comte"})
    # les 2 derniers, on les renomme, même si on va pas les utiliser, pour le drop

    # on enlève les colonnes qui nous serviront pas
    loc = loc.drop(columns=['FIPS', 'Confirmed', 'Deaths', 'Recovered',
                            'Active', 'Incidence_Rate', 'Case_Fatality_Ratio', 'Last_Update'])
    # on change le type de certains attributs pour que ca corresponde avec la base de données
    #loc['Province_State'] = loc['Province_State'].astype('|S80')
    #loc['Country_Region'] = loc['Country_Region'].astype('|S80')

    return loc


# recupère les données d'une certaine date et les insère dans Quantification
def importationQuantVersDb(sqlite_connection, date):
    url = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/" + date + ".csv"
    s = requests.get(url).content
    quant = pd.read_csv(io.StringIO(s.decode("utf-8")), delimiter=',')

    # on renomme les colonnes pour qu'elles correspondent à la base de données
    quant = quant.rename(columns={"Lat": "Latitude",
                                  "Long_": "Longitude",
                                  "Case-Fatality_Ratio": "Case_Fatality_Ratio",
                                  "Last_Update": "Date",
                                  "Incident_Rate": "Incidence_Rate"})

    # on enlève les colonnes qui nous serviront pas
    quant = quant.drop(columns=['FIPS', 'Admin2', 'Province_State',
                                'Country_Region', 'Latitude', 'Longitude'])

    # on change le type de certains attributs pour que ca corresponde avec la base de données
    quant['Date'] = pd.to_datetime(quant['Date'])
    quant['Active'] = quant['Active'].fillna(-1)  # met les NaN à 0
    quant['Active'] = quant['Active'].astype('int64')

    #Date est actuellement la date du last update
    #on veut la date à laquelle correpondent les données (la veille)
    for idr, row in quant.iterrows():
        quant.loc[idr, 'Date'] -= datetime.timedelta(days=1)

    quant.to_sql('Quantification', sqlite_connection, if_exists='append', index=False)

#si les données de nos tables sont à jour, on renvoie true, sinon false
def regardeSiDonneesAJour(dateFin):
    date = datetime.date.today()
    date = date - datetime.timedelta(days=1)  # date d'hier
    if dateFin == date:
        return True
    try:
        url = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/"
        temp = dateFin.strftime("%m-%d-%Y")
        urltemp = url + temp + ".csv"
        s = requests.get(urltemp).content
        c = pd.read_csv(io.StringIO(s.decode("utf-8")), delimiter=',')
    except KeyError: #les données à cette date existent pas
        date = date - datetime.timedelta(days=1) #date de l'avant veille
        if dateFin == date:
            return True
            #dans ce cas, la date max dans la table est la date de l'avant veille
            #on vient de voir qu'il n'y a pas d'entrées dans la base pour la veille
                #donc la date max est bien la date de la dèrnière entrée
        else:
            return False
            #dans ce cas, la date max n'est ni la date de la veille, ni celle de l'avant veille
            #donc il nous manque forcément des données
    return False
    #dans ce cas, les données existent pour la veille,
    #mais la date max ne correpond pas à celle de la veille
    #donc il manque des données



def importatio():
    # dateFin est la date "jusqu'à" laquelle on récupère des données
    dateFin = datetime.datetime(2020, 11, 19)
    dateFin = dateFin.date()

    # connection à bd
    engine = create_engine('sqlite:///covid.db', echo=True)
    sqlite_connection = engine.connect()

    # on cherche le nombre de tuples dans Localisation
    countL = sqlite_connection.execute("select count(*) from Localisation")
    countL = list(countL)
    countL = countL[0][0]

    if countL != 0:
        # si countL > 0, Localisation est remplie, et donc Quantification l'est aussi
        # du coup on va pas chercher la date jusqu'à laquelle on a des données
        # et on change dateFin (vu qu'on a déjà certaines données)
        dateMax = sqlite_connection.execute("select max(Date) from Quantification")
        dateMax = list(dateMax)
        dateMax = dateMax[0][0]
        dateMax = datetime.datetime.strptime(dateMax, '%Y-%m-%d %H:%M:%S.%f').date()
        dateFin = dateMax

        #on va regarder si les dernières données en lignes ont déjà été importées dans nos tables
        ret = regardeSiDonneesAJour(dateMax)
        if ret == True : #les données sont à jour
            return #on sort de la fonction


    # on regarde si des données ont été mises en ligne hier
    # si ce n'est pas le cas, on utilise celle d'avant-hier
    date = datetime.date.today()
    if countL == 0:
        # Localisation est vide, donc tables sont vides
            #on utilise importationLocVersDb pour remplir Localisation
        try:
            date = date - datetime.timedelta(days=1) #il ne peut pas y avoir de données pour aujourd'hui
            datestr = date.strftime("%m-%d-%Y")
            importationLocVersDb(sqlite_connection, datestr)
        except:
            # si on arrive là, c'est que les données de la veille sont pas encore en ligne
            date = date - datetime.timedelta(days=1) #date de l'avant veille
            datestr = date.strftime("%m-%d-%Y")
            importationLocVersDb(sqlite_connection, datestr)
    else:
        # Localisation n'est pas vide, donc tables remplies
            #on utilise majLoc pour mettre à jour Localisation
        date = date - datetime.timedelta(days=1) #il ne peut pas y avoir de données pour aujourd'hui
        datestr = date.strftime("%m-%d-%Y")
        maj = majLoc(sqlite_connection, datestr)
        if maj == False:
            # si on arrive là, c'est que les données de la veille sont pas encore en ligne
            date = date - datetime.timedelta(days=1) #date de l'avant veille
            datestr = date.strftime("%m-%d-%Y")
            maj = majLoc(sqlite_connection, datestr)


    # boucle à reculons pour ajouter des données à Quantifications
    while (date > dateFin):
        print(" ")
        datestr = date.strftime("%m-%d-%Y")
        importationQuantVersDb(sqlite_connection, datestr)
        date -= datetime.timedelta(days=1)

    sqlite_connection.close()




def DailyQuant():
    conn = sqlite3.connect('covid.db')

    df_uk = pd.read_sql_query("Select Date, Country_Region, Deaths, Recovered, Confirmed from Quantification natural join Localisation where Country_Region = 'United Kingdom' group by Date",conn)

    df_fr = pd.read_sql_query("Select Date, Country_Region, Deaths, Recovered, Confirmed from Quantification natural join Localisation where Country_Region = 'France' group by Date",conn)

    df_ja = pd.read_sql_query("Select Date, Country_Region, Deaths, Recovered, Confirmed from Quantification natural join Localisation where Country_Region = 'Japan' group by Date",conn)

    #print(df_uk)
    #print(df_fr)
    #print(df_ja)

    df_uk2 = df_uk.copy()
    df_fr2 = df_fr.copy()
    df_ja2 = df_ja.copy()


    for i in df_uk.index:
        if i > 0:
            df_uk['Deaths'][i] = df_uk2['Deaths'][i]- df_uk2['Deaths'][i-1]
            df_uk['Recovered'][i] = df_uk2['Recovered'][i] - df_uk2['Recovered'][i-1]
            df_uk['Confirmed'][i] = df_uk2['Confirmed'][i] - df_uk2['Confirmed'][i-1]

    for i in df_fr.index:
        if i > 0:
            df_fr['Deaths'][i] = df_fr2['Deaths'][i]- df_fr2['Deaths'][i-1]
            df_fr['Recovered'][i] = df_fr2['Recovered'][i] - df_fr2['Recovered'][i-1]
            df_fr['Confirmed'][i] = df_fr2['Confirmed'][i] - df_fr2['Confirmed'][i-1]

    for i in df_ja.index:
        if i > 0:
            df_ja['Deaths'][i] = df_ja2['Deaths'][i]- df_ja2['Deaths'][i-1]
            df_ja['Recovered'][i] = df_ja2['Recovered'][i] - df_ja2['Recovered'][i-1]
            df_ja['Confirmed'][i] = df_ja2['Confirmed'][i] - df_ja2['Confirmed'][i-1]

    df_uk = df_uk[1:][:]
    df_fr = df_fr[1:][:]
    df_ja = df_ja[1:][:]

    df_total = pd.concat([df_uk,df_fr], ignore_index=True ,sort = False)
    df_total = pd.concat([df_total,df_ja], ignore_index=True ,sort = False)

    #////Moyenne Glissante sur 7 jours /////
    #print(df_total)

    df_2 = df_total[['Date', 'Country_Region', 'Deaths']]
    df_4 = df_total[['Date', 'Country_Region', 'Recovered']]
    df_6 = df_total[['Date', 'Country_Region', 'Confirmed']]

    #print(df_2)

    df_2.drop_duplicates(subset=['Date', 'Country_Region'], inplace=True)
    df_4.drop_duplicates(subset=['Date', 'Country_Region'], inplace=True)
    df_6.drop_duplicates(subset=['Date', 'Country_Region'], inplace=True)

    df_3 = df_2.set_index(["Date", "Country_Region"])  # index
    df_3 = df_3.unstack().shift(1)  # pull out the groups, shift with lag step=1
    df_3 = df_3.stack(dropna=False)
    df_3.reset_index().sort_values("Country_Region")

    #print(df_3)

    df_5 = df_4.set_index(["Date", "Country_Region"])  # index
    df_5 = df_5.unstack().shift(1)  # pull out the groups, shift with lag step=1
    df_5 = df_5.stack(dropna=False)
    df_5.reset_index().sort_values("Country_Region")

    #print(df_5)

    df_7 = df_6.set_index(["Date", "Country_Region"])  # index
    df_7 = df_7.unstack().shift(1)  # pull out the groups, shift with lag step=1
    df_7 = df_7.stack(dropna=False)
    df_7  # stack the groups back, keep the missing values
    df_7.reset_index().sort_values("Country_Region")

    #print(df_7)

    deaths = df_2
    recovered = df_4
    confirmed = df_6

    for i in range(-3, 4):
        df_3 = df_2.set_index(["Date", "Country_Region"])
        df_3 = df_3.unstack().shift(i)
        df_3 = df_3.stack(dropna=True)
        df_3 = df_3.reset_index().sort_values("Country_Region")
        new_column_name = "Deaths" + '_' + '%s' % i
        df_3 = df_3.rename(columns={"Deaths": new_column_name})
        df_3.head()
        deaths = deaths.merge(df_3, on=['Date', 'Country_Region'])

    #print(deaths)

    for i in range(-3, 4):
        df_5 = df_4.set_index(["Date", "Country_Region"])
        df_5 = df_5.unstack().shift(i)
        df_5 = df_5.stack(dropna=True)
        df_5 = df_5.reset_index().sort_values("Country_Region")
        new_column_name = "Recovered" + '_' + '%s' % i
        df_5 = df_5.rename(columns={"Recovered": new_column_name})
        df_5.head()
        recovered = recovered.merge(df_5, on=['Date', 'Country_Region'])

    #print(recovered)

    for i in range(-3, 4):
        df_7 = df_6.set_index(["Date", "Country_Region"])
        df_7 = df_7.unstack().shift(i)
        df_7 = df_7.stack(dropna=True)
        df_7 = df_7.reset_index().sort_values("Country_Region")
        new_column_name = "Confirmed" + '_' + '%s' % i
        df_7 = df_7.rename(columns={"Confirmed": new_column_name})
        df_7.head()
        confirmed = confirmed.merge(df_7, on=['Date', 'Country_Region'])

    #print(confirmed)

    deaths['7_MVAVG_deaths'] = (deaths['Deaths_-3'] + deaths['Deaths_-2'] + deaths['Deaths_-1'] + deaths['Deaths_0'] +
                                deaths['Deaths_1'] + deaths['Deaths_2'] + deaths['Deaths_3']) / 7

    recovered['7_MVAVG_Recovered'] = (recovered['Recovered_-3'] + recovered['Recovered_-2'] + recovered['Recovered_-1'] + recovered['Recovered_0'] +
                                      recovered['Recovered_1'] + recovered['Recovered_2'] + recovered['Recovered_3']) / 7

    confirmed['7_MVAVG_Confirmed'] = (confirmed['Confirmed_-3']+confirmed['Confirmed_-2']+confirmed['Confirmed_-1']+confirmed['Confirmed_0']+
                                      confirmed['Confirmed_1']+confirmed['Confirmed_2']+confirmed['Confirmed_3'])/7

    df_Deaths = deaths[['Date', 'Country_Region', 'Deaths', '7_MVAVG_deaths']]
    df_Recovered = recovered[['Date', 'Country_Region', 'Recovered', '7_MVAVG_Recovered']]
    df_Confirmed = confirmed[['Date', 'Country_Region', 'Confirmed', '7_MVAVG_Confirmed']]

    df_totalf = df_Deaths.merge(df_Recovered, on=['Date', 'Country_Region'], how='outer')
    df_totalf = df_total.merge(df_Confirmed, on=['Date', 'Country_Region'], how='outer')


    df_totalf = df_totalf.fillna(0)
    #print(df_totalf)

    df_totalf.to_sql('DailyQuant', con=conn, if_exists='replace')

    conn.close()


def prediction():
    conn = sqlite3.connect('covid.db')

    # //////Prediction/////

    url = "https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/owid-covid-data.csv"
    p = requests.get(url).content
    df0 = pd.read_csv(io.StringIO(p.decode("utf-8")), delimiter=',', index_col=None)
    # df0 = pd.read_csv('owid-covid-data.csv', index_col=None)
    df0['date'] = pd.to_datetime(df0['date'])
    df = df0[['date', 'location', 'new_cases', 'new_deaths']]
    df.rename(columns={'date': 'Date', 'location': 'Country_Region', 'new_cases': 'Confirmed', 'new_deaths': 'Deaths'},
              inplace=True)
    df = df[(df['Country_Region'] == 'France') | (df['Country_Region'] == 'United Kingdom') | (
                df['Country_Region'] == 'Japan')]
    df['Country_Region'].unique()
    df = df.fillna(0)
    df_deaths = df.drop(columns={'Confirmed'})
    df_confirmed = df.drop(columns={'Deaths'})

    df_confirmed_4 = df_confirmed
    for i in range(-3, 4):
        df_confirmed_2 = df_confirmed.set_index(["Date", "Country_Region"])
        df_confirmed_3 = df_confirmed_2.unstack().shift(-i)
        df_confirmed_3 = df_confirmed_3.stack(dropna=True)
        df_confirmed_3 = df_confirmed_3.reset_index().sort_values(
            'Country_Region')  # by=['Country_Region', 'Date'], ascending=True)
        new_column_name = "Confirmed" + '_' + '%s' % i
        df_confirmed_3 = df_confirmed_3.rename(columns={"Confirmed": new_column_name})
        df_confirmed_4 = df_confirmed_4.merge(df_confirmed_3, on=['Date', 'Country_Region'], how='outer')

    df_confirmed_4 = df_confirmed_4.sort_values(by=['Country_Region', 'Date'], ascending=True)
    lr = LinearRegression()

    # ///Pour la France/////
    df_2 = df_confirmed_4[(df_confirmed_4['Country_Region'] == 'France') & (df_confirmed_3['Date'] > '2020-03-10')]

    ## Join with total cases
    total_cases = df0[['total_cases', 'date', 'location']]
    total_cases = total_cases.rename(
        columns={'total_cases': 'total_confirmed', 'date': 'Date', 'location': 'Country_Region'})

    df_3 = df_2.merge(total_cases, on=['Date', 'Country_Region'], how='inner')

    # print(df_3)

    df_3['Date'] = df_3['Date'].dt.date
    df_5 = df_3[:][-1:]
    last_reporting_date = df_3['Date'][-1:].values[0]
    X_pred = df_5[['Confirmed', 'Confirmed_-1', 'Confirmed_-2', 'Confirmed_-3', 'total_confirmed']].values

    # print(df_5)

    import datetime

    day_1 = last_reporting_date + datetime.timedelta(days=1)
    day_2 = last_reporting_date + datetime.timedelta(days=2)
    day_3 = last_reporting_date + datetime.timedelta(days=3)

    # Create dataframe for results
    prediction_results = pd.DataFrame()
    prediction_results['Date'] = [day_1, day_2, day_3]

    confirmed_pred = []
    for i in range(1, 4):
        # Define target variable
        y_var = 'Confirmed_' + '%s' % i
        # Remove dates that can't be used for training
        df_4 = df_3[:][:-i]
        df_4.reset_index(inplace=True)
        # Define target and explanatory variables
        Y = df_4[y_var].values
        X = df_4[['Confirmed', 'Confirmed_-1', 'Confirmed_-2', 'Confirmed_-3', 'total_confirmed']].values
        # Estimate linear regression model
        lr.fit(X, Y)
        # print('For model %s, the score is: %s' %(i,lr.score(X,Y)))
        # Predict for the relevant future date:
        confirmed_pred.append(lr.predict(X_pred)[0])

    prediction_results['Country_Region'] = 'France'
    prediction_results['Confirmed_prediction'] = confirmed_pred
    # print(prediction_results)

    # ///// pour United Kingdom////

    df_Uk = df_confirmed_4[
        (df_confirmed_4['Country_Region'] == 'United Kingdom') & (df_confirmed_4['Date'] > '2020-06-10')]
    df_Uk_2 = df_Uk.merge(total_cases, on=['Date', 'Country_Region'], how='inner')
    df_Uk_2['Date'] = df_Uk_2['Date'].dt.date

    df_Uk_3 = df_Uk_2[:][-1:]
    last_reporting_date_2 = df_Uk_2['Date'][-1:].values[0]
    X_predUK = df_Uk_3[['Confirmed', 'Confirmed_-1', 'Confirmed_-2', 'Confirmed_-3', 'total_confirmed']].values

    day_1 = last_reporting_date_2 + datetime.timedelta(days=1)
    day_2 = last_reporting_date_2 + datetime.timedelta(days=2)
    day_3 = last_reporting_date_2 + datetime.timedelta(days=3)

    prediction_results_Uk = pd.DataFrame()
    prediction_results_Uk['Date'] = [day_1, day_2, day_3]
    lr2 = LinearRegression()

    confirmed_predUK = []
    for i in range(1, 4):
        # Define target variable
        y_var = 'Confirmed_' + '%s' % i
        # Remove dates that can't be used for training
        df_Uk_4 = df_Uk_2[:][:-i]
        df_Uk_4.reset_index(inplace=True)
        # Define target and explanatory variables
        YUK = df_Uk_4[y_var].values
        XUK = df_Uk_4[['Confirmed', 'Confirmed_-1', 'Confirmed_-2', 'Confirmed_-3', 'total_confirmed']].values
        # Estimate linear regression model
        lr2.fit(XUK, YUK)
        # print('For model %s, the score is: %s' %(i,lr.score(X,Y)))
        # Predict for the relevant future date:
        confirmed_predUK.append(lr2.predict(X_predUK)[0])

    prediction_results_Uk['Country_Region'] = 'United Kingdom'
    prediction_results_Uk['Confirmed_prediction'] = confirmed_predUK
    print(prediction_results_Uk)

    # /////Pour le Japan////

    df_Ja = df_confirmed_4[(df_confirmed_4['Country_Region'] == 'Japan') & (df_confirmed_4['Date'] > '2020-03-10')]
    df_Ja_2 = df_Ja.merge(total_cases, on=['Date', 'Country_Region'], how='inner')
    df_Ja_2['Date'] = df_Ja_2['Date'].dt.date

    df_Ja_3 = df_Ja_2[:][-1:]
    last_reporting_date_3 = df_Ja_2['Date'][-1:].values[0]
    X_predJa = df_Ja_3[['Confirmed', 'Confirmed_-1', 'Confirmed_-2', 'Confirmed_-3', 'total_confirmed']].values

    day_1 = last_reporting_date_3 + datetime.timedelta(days=1)
    day_2 = last_reporting_date_3 + datetime.timedelta(days=2)
    day_3 = last_reporting_date_3 + datetime.timedelta(days=3)

    prediction_results_Ja = pd.DataFrame()
    prediction_results_Ja['Date'] = [day_1, day_2, day_3]

    confirmed_predJa = []
    for i in range(1, 4):
        # Define target variable
        y_var = 'Confirmed_' + '%s' % i
        # Remove dates that can't be used for training
        df_Ja_4 = df_Ja_2[:][:-i]
        df_Ja_4.reset_index(inplace=True)
        # Define target and explanatory variables
        YJa = df_Ja_4[y_var].values
        XJa = df_Ja_4[['Confirmed', 'Confirmed_-1', 'Confirmed_-2', 'Confirmed_-3', 'total_confirmed']].values
        # Estimate linear regression model
        lr2.fit(XJa, YJa)
        # print('For model %s, the score is: %s' %(i,lr.score(X,Y)))
        # Predict for the relevant future date:
        confirmed_predJa.append(lr2.predict(X_predJa)[0])

    prediction_results_Ja['Country_Region'] = 'Japan'
    prediction_results_Ja['Confirmed_prediction'] = confirmed_predJa
    # print(prediction_results_Ja)

    df_predict = pd.concat([prediction_results, prediction_results_Uk], ignore_index=True, sort=False)
    df_predict = pd.concat([df_predict, prediction_results_Ja], ignore_index=True, sort=False)

    print(df_predict)

    df_predict.to_sql('Prediction', con=conn, if_exists='replace')

    conn.close()














