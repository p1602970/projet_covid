"""
    Ce fichier implémente la fonction qui permet de récupérer les données nécessaires
à l'affichage de la carte.
    Il nous faut un dictionnaire avec des données pour chaque pays (le om du pays est clé)
Certains noms de pays sont modifiés pour qu'ils correspondent plus tard avec ceux d'un
autre dictionnaire qu'ont utilisera pour afficher la carte.
    Il nous faut aussi un dictionnaire d'exceptions. Il s'agit là de "provinces" dont
le nom est le même que celui de leurs pays. Ce sera utile pour l'affichage.

"""

def recupPaysDeath(cursor):

    #on récupère les données pour les pays qui n'ont pas de provinces
    cursor.execute("select Country_Region, Deaths, Active, Incidence_Rate, Case_Fatality_Ratio from Quantification natural join Localisation "
                   "where Province_State is null and Date = ("
                   "select max(Date) from Quantification); ")
    listePaysDeaths = cursor.fetchall()
    dicoPaysDeaths = {}
    for pays, deaths, active, ir, cfr in listePaysDeaths:
        if(pays=='Congo (Kinshasa)'):
            dicoPaysDeaths['Republic of the Congo'] = [deaths, active, ir, cfr]
        if (pays == 'Congo (Brazzaville)'):
            dicoPaysDeaths['Democratic Republic of the Congo'] = [deaths, active, ir, cfr]
        if (pays == 'Czechia'):
            dicoPaysDeaths['Czech Republic'] = [deaths, active, ir, cfr]
        if (pays == 'Serbia'):
            dicoPaysDeaths['Republic of Serbia'] = [deaths, active, ir, cfr]
        if (pays == 'North Macedonia'):
            dicoPaysDeaths['Macedonia'] = [deaths, active, ir, cfr]
        if (pays == 'Taiwan*'):
            dicoPaysDeaths['Taiwan'] = [deaths, active, ir, cfr]
        if (pays == 'Korea, South'):
            dicoPaysDeaths['South Korea'] = [deaths, active, ir, cfr]
        if (pays == 'Burma'):
            dicoPaysDeaths['Myanmar'] = [deaths, active, ir, cfr]
        if (pays == 'Tanzania'):
            dicoPaysDeaths['United Republic of Tanzania'] = [deaths, active, ir, cfr]
        if (pays == "Cote d'Ivoire"):
            dicoPaysDeaths['Ivory Coast'] = [deaths, active, ir, cfr]
        if (pays == 'Guinea-Bissau'):
            dicoPaysDeaths['Guinea Bissau'] = [deaths, active, ir, cfr]
        dicoPaysDeaths[pays] = [deaths, active, ir, cfr]

    #on récupère les données pour les pays qui n'ont pas de comte
        #d'abord on cherche les pays qui ont de provinces_states et dont aucun province_state n'a de comte
    cursor.execute("select distinct Country_Region "
                   "from Localisation "
                   "where Province_State is not null "
                   "and Country_Region not in (select distinct Country_Region from Localisation where comte is not null );"
                   "")
    listePaysDeaths2 = cursor.fetchall()

    exception = {}
    for pays in listePaysDeaths2:
        #pour chaque pays trouve, on cherche si il y a un tuple ou Province state est nulle
            #si il y a une province nulle, c'est que le pays est pas divisé en provinces, mais qu'il existe des "outre mers"
            #on va traiter différemment les pays de ce genre
        cursor.execute("select count(*) from Localisation "
                       "where Country_Region='{pays}' "
                       "and Province_State is null;".format(pays=pays[0]))
        n = cursor.fetchall()
        n = n[0][0]
        if n==0: #plsrs province mais aucune nulles (tous dans le pays)
            cursor.execute("select sum(Deaths), sum(Active), avg(Incidence_Rate), avg(Case_Fatality_Ratio) "
                           "from Localisation natural join Quantification "
                           "where Country_Region = '{pays}' "
                           "and Date = (Select max(Date) from Quantification )"
                           .format(pays=pays[0]))
            d = cursor.fetchall()
            d = d[0]
            dicoPaysDeaths[pays[0]] = [d[0], d[1], d[2], d[3]]

        elif n==1: #plusieurs "provinces" mais en vrai c'est en dehors du territoire principale
                    #France et Denmark
            cursor.execute("select Province_State, Deaths, Active, Incidence_Rate, Case_Fatality_Ratio "
                           "from Localisation natural join Quantification "
                           "where Country_Region = '{pays}' "
                           "and Date = (Select max(Date) from Quantification )"
                           .format(pays=pays[0]))
            listeProvDeaths = cursor.fetchall()
            listeProvDeaths = listeProvDeaths
            for prov, deaths, active, ir, cfr in listeProvDeaths:
                dicoPaysDeaths[prov] = [deaths, active, ir, cfr]
                if type(prov) != str:
                    exception[pays[0]] = pays[0]
                exception[prov] = pays[0]


    #maintenant on cherche les pays avec des comtes
    cursor.execute("select distinct Country_Region "
                   "from Localisation "
                   "where Comte is not null ")
    listePaysDeaths3 = cursor.fetchall()
    #normalememnt il y a que les US, mais on fait une boucle au cas ou
    for pays in listePaysDeaths3:
        cursor.execute("select sum(Deaths), sum(Active), avg(Incidence_Rate), avg(Case_Fatality_Ratio) "
                       "from Localisation natural join Quantification "
                       "where Country_Region = '{pays}' "
                       "and Date = (Select max(Date) from Quantification )"
                       .format(pays=pays[0]))
        d = cursor.fetchall()
        d = d[0]
        #les US sont un cas particuliers... faut changer le nom pour que ce soit reconnu
        if pays[0] == 'US':
            dicoPaysDeaths['United States of America'] = [d[0], d[1], d[2], d[3]]

    return dicoPaysDeaths, exception
