function map(n){
    document.getElementById(`containerMap`).innerHTML = ``;
    //on récupère le dico avec les pays et les morts
    const paysDeath = JSON.parse(
        document.getElementById('pays-deaths').textContent
    );
    const exception = JSON.parse(
        document.getElementById('exception').textContent
    );
    console.log(paysDeath);
    console.log(exception);

    //on récupère les pays et leurs code à 3 lettres
    let worldtopo = Datamap.prototype.worldTopo
    worldtopo = worldtopo.objects.world.geometries;
    worldtopo = worldtopo.map(geo => {
        let obj = new Object;
        obj.id = geo.id;
        obj.name = geo.properties.name;
        return obj;
    });
    console.log(worldtopo)

    let series =[];
    worldtopo.map(tuple => {
        if (paysDeath.hasOwnProperty(tuple['name']))
            series.push([tuple['id'], paysDeath[tuple['name']][0],
                paysDeath[tuple['name']][1], paysDeath[tuple['name']][2],
                paysDeath[tuple['name']][3]]);

    });
    setMap(series, exception, n)
}

function setMap(series, exception, n){
    var dataset = {};

    // chaque pays aura une couleur en fonction de ses Deaths
    //on veut faire une palette de couleurs (avec les valeurs min et max de Deaths)
    var onlyValues = series.map(obj => obj[n]);
    var minValue = Math.min.apply(null, onlyValues),
            maxValue = Math.max.apply(null, onlyValues);

    // on fait la palette de couleurs
    var paletteScale = d3.scale.linear()
            .domain([minValue,maxValue])
            .range(["#EFEFFF","#363C7D"]); // blue color

    // on remplit dataset
    series.forEach(function(item){
        var iso = item[0],
                value = item[n];
        dataset[iso] = { deaths: item[1],
            fillColor: paletteScale(value),
            active: item[2],
            ir : Math.round(item[3]*100)/100,
            cfr : Math.round(item[4]*100)/100,
        };
    });

    // affichage map
    new Datamap({
        element: document.getElementById(`containerMap`),
        projection: 'mercator', // big world mapl
        fills: { defaultFill: '#fefefe' }, //pour pays pas renseignés
        data: dataset,
        geographyConfig: {
            borderColor: '#c4c9c9',
            highlightBorderWidth: 2,
            // pour pas changer couleur quand on hover
            highlightFillColor: function(geo) {
                return geo['fillColor'] || '#F5F5F5';
            },
            highlightBorderColor: '#B7B7B7',
            // pour le popup
            popupTemplate: function(geo, data) {
                //si pays pas dans dataset, on met pas de popup
                if (!data) { return ; }
                // sinon :
                return ['<div class="hoverinfo">',
                    '<strong>', geo.properties.name, '</strong>',
                    '<br>Deaths: <strong>', data.deaths, '</strong>',
                    '<br>Actives: <strong>', data.active, '</strong>',
                    '<br>Incidence: <strong>', data.ir, '</strong>',
                    '<br>Mortalité: <strong>', data.cfr, '</strong>',
                    '</div>'].join('');
            }
        },
        done: function(datamap) {
          datamap.svg.selectAll('.datamaps-subunit').on('click', function(geography) {
              if (exception.hasOwnProperty(geography.properties.name)){
                  link = exception[geography.properties.name] + "/" + geography.properties.name;
              } else {
                  switch (geography.properties.name){
                  case 'Republic of the Congo': link = 'Congo (Kinshasa)'; break;
                  case 'Democratic Republic of the Congo': link = 'Congo (Brazzaville)'; break;
                  case 'Czech Republic': link = 'Czechia'; break;
                  case 'Republic of Serbia': link = 'Serbia'; break;
                  case 'Macedonia': link = 'North Macedonia'; break;
                  case 'Taiwan': link = 'Taiwan*'; break;
                  case 'South Korea': link = 'Korea, South'; break;
                  case 'Myanmar': link = 'Burma'; break;
                  case 'United Republic of Tanzania': link = 'Tanzania'; break;
                  case 'Ivory Coast': link = "Cote d'Ivoire"; break;
                  case 'Guinea Bissau': link = 'Guinea-Bissau'; break;
                  case 'United States of America': link = 'US'; break;
                  default : link = geography.properties.name;
                  }
              }
              location.href = link;
          });
        }
    });
}


