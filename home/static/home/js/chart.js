function initChart(){
    let listeDeath = JSON.parse(document.getElementById('listeDeath').textContent);
    let listeIR = JSON.parse(document.getElementById('listeIR').textContent);
    const listeDate = JSON.parse(document.getElementById('listeDate').textContent);
    listeDeath = listeDeath.map(n => n/1000000);
    listeIR = listeIR.map(ir => Math.round(ir*100)/100);

    var ctx = document.getElementById('chartDeath').getContext('2d');
    let lab = 'nombre de décès (en millions)'
    let titre = 'Evolution morts sur les 30 derniers jours'
    chart(listeDeath, listeDate, ctx, lab, titre, true);

    ctx = document.getElementById('chartIR').getContext('2d');
    lab = 'taux d\'incidence';
    titre = 'Evolution du taux d\'incidence sur les 30 derniers jours';
    chart(listeIR, listeDate, ctx, lab, titre, false);
}

//listeA pour abscisses et listeO pour ordonnées
function chart(listeA, listeO, ctx, lab, titre, ligne){
        var chart = new Chart(ctx, {
            type: 'line',

            data: {
                labels: listeO,
                datasets: [{
                    label: lab,
                    borderColor: 'rgb(255, 99, 132)',
                    data: listeA
                }]
            },

            //Configuration
            options: {
                title: {
                display: true,
                    position: 'bottom',
                text: titre
                },
                showLines: ligne
            }
        });
}